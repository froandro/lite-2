#! /usr/bin/env bash

# Install Mysql
echo "----- Install Mysql ------"
apt update
apt install mysql-server -y

# Config Databases
echo "----- Config Databases ------"
mysql -uroot -e "CREATE DATABASE IF NOT EXISTS $DB_NAME DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
mysql -uroot -e "CREATE USER IF NOT EXISTS '$DB_USER'@'%' IDENTIFIED BY '$DB_PASSWORD';"
mysql -uroot -e "GRANT ALL ON $DB_NAME.* TO '$DB_USER'@'%';"
mysql -uroot -e "FLUSH PRIVILEGES;"

# Permission to connect from any address
echo "----- Permission to connect from any address ------"
sed -i 's/127.0.0.1/0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf


service mysql restart
echo "----- SUCCESSFUL -----"