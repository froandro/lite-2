#! /usr/bin/env bash

# Install Nginx Proxy
echo "----- Install Nginx Proxy ------"
sudo apt update
sudo apt install nginx -y

cat > /etc/nginx/nginx.conf <<EOF
user www-data;
worker_processes 1;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
        worker_connections 768;
}
http {
    upstream myweb {
        server $ip_web;
        server $ip_web2;
        server $ip_web3;
    }
    server {
        location / {
            proxy_pass 'http://myweb/';
            # Управление заголовками на прокси сервере
            proxy_set_header X-Forwarded-Host \$host;
            proxy_set_header X-Forwarded-Proto http;
            proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP \$remote_addr;
        }
    }
}
EOF

sudo nginx -t
sudo systemctl restart nginx
echo "----- SUCCESSFUL -----"